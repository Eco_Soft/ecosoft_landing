var express = require('express');
var serveStatic = require('serve-static');
var app = express();
var port = process.env.PORT || 3000;
var msg = "Server starter on port : " + port;
app.use(serveStatic('public', {'index': ['index.html', 'default.htm']}));
app.listen(port);
console.log(msg);
